""" app.py """

from flask import Flask
app = Flask(__name__)

def factorial(n):
    """ factorial calculation """

    if not isinstance(n, int):
        return "wrong type"
    if n < 1:
        return "wrong number"
    if n == 1:
        return 1
    return n * factorial(n - 1)

# entrypoint
@app.route('/')
def index():
    """Entry point"""
    x = 5
    result = factorial(x)
    if isinstance(result, int):
        resultStr = str(x) + "! = " + str(result)
    else:
        resultStr = result
    return resultStr

if __name__ == '__main__':
    app.run(threaded=True, port=5000)
