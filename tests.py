""" file for tests """
from app import factorial

def test_method():
    """ test! """
    assert factorial(5) == 120
    assert factorial(-1) == 'wrong number'
    assert factorial(1.2) == 'wrong type'
    assert factorial("ciao") == 'wrong type'
